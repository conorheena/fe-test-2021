/* eslint-disable */

context('User role', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('Should see a list of 8 users', () => {
    cy.get('li').should('have.length', 8);
  });

  it('Should see one Admin user in the list', () => {
    cy.get('.MuiSelect-select').click();
    cy.get('.MuiListItem-root')
      .contains('Admin')
      .click();
    cy.get('li').should('have.length', 1);
  });

  it('Should see six Broker users in the list', () => {
    cy.get('.MuiSelect-select').click();
    cy.get('.MuiListItem-root')
      .contains('Broker')
      .click();
    cy.get('li').should('have.length', 6);
  });

  it('Should see two Advisor users in the list', () => {
    cy.get('.MuiSelect-select').click();
    cy.get('.MuiListItem-root')
      .contains('Advisor')
      .click();
    cy.get('li').should('have.length', 2);
  });
});
