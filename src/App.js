import React from 'react';
import styled from 'styled-components';
import { useQuery, gql } from '@apollo/client';
import { createGlobalStyle } from 'styled-components';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import logo from './svg/acre-logo.svg';
import { UserItemList } from './components/userItemList';

const USER_QUERY = gql`
  query($role: String!) {
    users(role: $role) {
      name
      role
      permissions {
        createCustomer
      }
    }
  }
`;

const App = () => {
  const [selectedRole, setSelectedRole] = React.useState('');

  const { data, isLoading, error } = useQuery(USER_QUERY, {
    variables: {
      role: selectedRole,
    },
  });

  const handleChange = (e) => {
    const role = e.target.value;
    setSelectedRole(role);
  };

  return (
    <AppWrapper data-testid="app">
      <Container>
        <header className="app-header">
          <img src={logo} className="app-logo" alt="logo" />

          <Title>Welcome to acre</Title>

          <h2>Users</h2>

          <StyledFormControl>
            <InputLabel htmlFor="user-role-select">User role</InputLabel>
            <StyledSelect
              id="user-role-select"
              value={selectedRole}
              onChange={(e) => handleChange(e)}
            >
              <MenuItem value="">Any</MenuItem>
              <MenuItem value="ADMIN">Admin</MenuItem>
              <MenuItem value="BROKER">Broker</MenuItem>
              <MenuItem value="ADVISOR">Advisor</MenuItem>
            </StyledSelect>
          </StyledFormControl>

          <UserItemList
            users={(data && data.users) || []}
            isLoading={isLoading}
            error={error}
          />
        </header>
      </Container>

      <GlobalStyle />
    </AppWrapper>
  );
};

const AppWrapper = styled.div`
  font-family: 'Roboto', sans-serif;

  .app-logo {
    height: 3rem;
  }

  .app-header {
    background-color: white;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    color: #333333;
    padding: 10px;
  }
`;

const Title = styled.h1`
  font-family: 'Playfair Display', serif;
  font-size: 2rem;
`;

const StyledFormControl = styled(FormControl)`
  max-width: calc(100% - 10px);
`;

const StyledSelect = styled(Select)`
  width: 300px;
  max-width: 100%;
`;

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 2rem;
    font-size: 14px;
  }
`;

export default App;
