import React from 'react';
import styled from 'styled-components';
import { UserItem } from './userItem';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';
import List from '@material-ui/core/List';

const UserItemList = ({ users, isLoading, error }) => {
  if (isLoading || !users)
    return <StyledCircularProgress data-testid="loadingSpinner" />;

  if (error)
    return (
      <StyledAlert data-testid="errorAlert" severity="error">
        Sorry, there was an error loading the users
      </StyledAlert>
    );

  return (
    <StyledList data-testid="userItemList">
      {users.map((user) => {
        return (
          <li key={user.name}>
            <UserItem
              name={user.name}
              role={user.role}
              permissions={user.permissions}
            />
          </li>
        );
      })}
    </StyledList>
  );
};

const StyledList = styled(List)`
  width: 100%;
`;

const StyledCircularProgress = styled(CircularProgress)`
  margin: 30px auto;
`;

const StyledAlert = styled(Alert)`
  margin: 30px auto;
`;

export { UserItemList };
