import React from 'react';
import styled from 'styled-components';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';

const UserItem = ({ name, role, permissions }) => {
  const createCustomer = permissions && permissions.createCustomer;

  return (
    <StyledListItem
      data-testid="listItem"
      button
      disableRipple
      disableTouchRipple
    >
      <ListItemText primary={`${name} - ${role.join(', ')}`} />

      {createCustomer && (
        <Button
          data-testid="createCustomerButton"
          variant="outlined"
          color="primary"
        >
          Create customer
        </Button>
      )}
    </StyledListItem>
  );
};

const StyledListItem = styled(ListItem)`
  @media (max-width: 768px) {
    text-align: center;
    flex-direction: column;
    align-items: center;
  }
`;

export { UserItem };
