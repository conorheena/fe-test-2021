/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import { UserItem } from '../userItem';
import { render, screen } from '@testing-library/react';

describe('<UserItem />', () => {
  test('renders list item', () => {
    render(<UserItem name={'some name'} role={['ADMIN']} permissions={{}} />);
    const listItem = screen.getByTestId('listItem');
    expect(listItem).toBeInTheDocument();
  });

  describe('when user has createCustomer permission', () => {
    test('renders create customer button', () => {
      render(
        <UserItem
          name={'some name'}
          role={['ADMIN']}
          permissions={{ createCustomer: true }}
        />,
      );
      const createCustomerButton = screen.queryByTestId('createCustomerButton');
      expect(createCustomerButton).toBeInTheDocument();
    });
  });

  describe('when user does not have createCustomer permission', () => {
    test('does not render create customer button', () => {
      render(<UserItem name={'some name'} role={['ADMIN']} permissions={{}} />);
      const createCustomerButton = screen.queryByTestId('createCustomerButton');
      expect(createCustomerButton).not.toBeInTheDocument();
    });
  });
});
