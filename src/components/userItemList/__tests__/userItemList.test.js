/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import { UserItemList } from '../userItemList';
import { render, screen } from '@testing-library/react';

const ADMIN = 'ADMIN';
const BROKER = 'BROKER';
const ADVISOR = 'ADVISOR';

export const mockUsers = [
  {
    name: 'Ryu',
    role: [ADMIN],
    createdAt: 'Fri Jan 18 2019 13:10:20 GMT+0000 (Greenwich Mean Time)',
    permissions: {
      createCustomer: true,
    },
  },
  {
    name: 'Ken',
    role: [ADVISOR, BROKER],
    createdAt: 'Fri Jan 18 2018 09:10:20 GMT+0000 (Greenwich Mean Time)',
    permissions: {
      createCustomer: true,
    },
  },
  {
    name: 'Vega',
    role: [BROKER],
    createdAt: 'Fri Jan 18 2018 09:10:20 GMT+0000 (Greenwich Mean Time)',
    permissions: {},
  },
  {
    name: 'Guile',
    role: [ADVISOR],
    createdAt: '',
    permissions: {},
  },
];

describe('<UserItemList />', () => {
  describe('when there is an error', () => {
    test('renders error message', () => {
      render(<UserItemList users={mockUsers} error />);
      const errorMessage = screen.getByTestId('errorAlert');
      expect(errorMessage).toBeInTheDocument();
    });
  });

  describe('when there is an error', () => {
    test('renders error message', () => {
      render(<UserItemList users={mockUsers} error />);
      const errorMessage = screen.getByTestId('errorAlert');
      expect(errorMessage).toBeInTheDocument();
    });
  });

  describe('when isLoading is true', () => {
    test('renders error message', () => {
      render(<UserItemList users={mockUsers} isLoading />);
      const loadingSpinner = screen.getByTestId('loadingSpinner');
      expect(loadingSpinner).toBeInTheDocument();
    });
  });

  describe('when isLoading and error are false', () => {
    test('renders userlist', () => {
      render(<UserItemList users={mockUsers} />);
      const userItemList = screen.getByTestId('userItemList');
      expect(userItemList).toBeInTheDocument();
    });

    test('renders a list of 4 users', () => {
      render(<UserItemList users={mockUsers} />);
      const userItemList = screen.queryAllByTestId('listItem');
      expect(userItemList).toHaveLength(4);
    });
  });
});
