import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import App from '../App';
import { render, screen } from '@testing-library/react';

const mocks = [];

describe('<App />', () => {
  test('renders without crashing', () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <App />
      </MockedProvider>,
    );
    const app = screen.getByTestId('app');
    expect(app).toBeInTheDocument();
  });
});
